# Scripts
## a collection of usefull scripts.
## Containing:
 - file-encryption [script](https://github.com/xNaCly/scripts/tree/master/encrypt)
 - several [scripts](https://github.com/xNaCly/scripts/tree/master/discord) regarding [Discord](https://discordapp.com/company)
 - two [scripts](https://github.com/xNaCly/scripts/tree/master/dsb) to get plans from [DSB](https://digitales-schwarzes-brett.de/)
 - Text formatting [scripts](https://github.com/xNaCly/scripts/tree/master/Text%20Formatting)

## General Info:

- `written and tested on win10_64x using python3.8`<br>
- all scripts are `OS` independent if not stated differently
  <br>
- For private usage.
- docs `0%[==> ]₇₅%` done
